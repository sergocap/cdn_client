$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "cdn_client/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "cdn_client"
  s.version     = CdnClient::VERSION
  s.authors     = ["sergocap"]
  s.email       = ["systemofadown.2013@yandex.ru"]
  s.homepage    = "https://gitlab.com/sergocap/cdn_client"
  s.summary     = "-"
  s.description = "-"
  s.license     = "MIT"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.md"]

  s.add_dependency "rails", "~> 5.0.2"

  s.add_development_dependency "bundler"
  s.add_dependency "activesupport"
  s.add_dependency "configliere"
  s.add_dependency "redis"
  s.add_dependency "hashie"
  s.add_dependency "rest-client"
end
