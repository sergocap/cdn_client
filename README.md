# CdnClient
Short description and motivation.

## Usage
Для рендеринга решения: прописываем в settings.yml redis: cdn: 'redis_url' ; render_cdn(:key)

## Installation
Add this line to your application's Gemfile:

```ruby
gem 'cdn_client'
```

And then execute:
```bash
$ bundle
```

Or install it yourself as:
```bash
$ gem install cdn_client
```

## Contributing
Contribution directions go here.

## License
The gem is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).
