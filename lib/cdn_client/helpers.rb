require 'active_support/concern'

module CdnClient::ActionView
  module Helpers
    def render_cdn(key)
      data = CdnClient::Consumer.new(key: key).get_data
      result = ERB.new(data.template).result(binding)
      result.gsub('&quot;', '"').gsub('&gt;', '>').html_safe
    end

    ActiveSupport.on_load :action_view do
      include CdnClient::ActionView::Helpers
    end
  end
end
