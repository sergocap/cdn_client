require "cdn_client/version"
require "cdn_client/helpers"
require "hashie"

module CdnClient
  METHOD_PREFIX = %w(css js template settings).freeze

  class Consumer
    attr_accessor :key, :css, :template, :js, :settings, :data

    def initialize(key:)
      @key = key
    end

    def get_data
      consume_base
      Hashie::Mash.new METHOD_PREFIX.each_with_object({}){|mp, hash| hash[mp] = send "#{mp}"}
    end

    private
    def redis
      @@redis ||= Redis.new(url: Settings['redis.cdn'])
    end

    def consume_base
      METHOD_PREFIX.each do |method_prefix|
        result = send "consume_#{method_prefix}"
        unless result
          request = JSON.parse RestClient.post("#{Settings['profile.url']}/cdn/update", {key: key})
          raise 'server error' if request['result'] != 'ok'
          result = send "consume_#{method_prefix}"
        end
        result
      end
    end

    def consume_css
      @css = redis.get "cdn:css:#{key}"
    end

    def consume_js
      @js = redis.get "cdn:js:#{key}"
    end

    def consume_template
      @template = redis.get "cdn:templates:#{key}"
    end

    def consume_settings
      @settings = Hashie::Mash.new JSON.load(redis.get("cdn:settings:#{key}"))
    end
  end
end
